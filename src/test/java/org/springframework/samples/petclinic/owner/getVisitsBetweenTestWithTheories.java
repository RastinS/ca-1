package org.springframework.samples.petclinic.owner;

import org.junit.Assume;
import org.junit.Before;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;
import org.springframework.samples.petclinic.visit.Visit;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Theories.class)
public class getVisitsBetweenTestWithTheories {

	private Pet pet;
	@Before
	public void setup() {
		pet = new Pet();
		pet.setBirthDate(LocalDate.of(2009, 1, 8));
		for(Visit visit: PetTest.makeMockVisits()) {
			pet.addVisit(visit);
		}
	}

	@DataPoints
	public static LocalDate[] dates() {
		return new LocalDate[] {
			LocalDate.of(2010, 1, 1),
			LocalDate.of(2010, 1, 3),
			LocalDate.of(2012, 1, 3),
			LocalDate.of(2015, 1, 3),
			LocalDate.of(2018, 1, 3),
			LocalDate.of(2020, 1, 3),
			LocalDate.of(2021, 1, 1)
		};
	}

	@Theory
	public void testVisitsBetweenDates(LocalDate startDate, LocalDate endDate) {
		Assume.assumeTrue(endDate.isAfter(startDate));
		int numOfVisitsBetweenDates = 0;

		for(Visit visit: PetTest.makeMockVisits()) {
			LocalDate visitDate = visit.getDate();
			if(visitDate.isAfter(startDate) && visitDate.isBefore(endDate))
				numOfVisitsBetweenDates++;
		}
		assertThat(pet.getVisitsBetween(startDate, endDate).size()).isEqualTo(numOfVisitsBetweenDates);
	}
}
