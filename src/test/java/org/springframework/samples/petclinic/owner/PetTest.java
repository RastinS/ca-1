package org.springframework.samples.petclinic.owner;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.samples.petclinic.visit.Visit;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class PetTest {

	private static Pet mockPet;

	private static Owner mockOwner;

	private static PetType mockPetType;

	private static ArrayList<Visit> mockVisits;

	@Before
	public void setup() {
		mockOwner = makeMockOwner();
		mockPetType = makeMockPetType();
		mockVisits = makeMockVisits();
		mockPet = makeMockPet();
	}

	@Test
	public void testPetSimpleSetAndGet() {
		Pet pet = new Pet();
		pet.setBirthDate(LocalDate.of(2020, 1, 8));
		pet.setOwner(mockOwner);
		pet.setType(mockPetType);
		pet.setName("shakespeare");

		assertThat(pet.getBirthDate()).isEqualTo(LocalDate.of(2020, 1, 8));
		assertThat(pet.getOwner().getFirstName()).isEqualTo("rastin");
		assertThat(pet.getOwner().getLastName()).isEqualTo("soraki");

		assertThat(pet.getType().getName()).isEqualTo("Cat");
		assertThat(pet.getName()).isEqualTo("shakespeare");
	}

	@Test
	public void testSortingVisitDates() {
		List<Visit> petVisits = mockPet.getVisits();
		int i = 0;
		int j = mockVisits.size() - 1;
		while(i < petVisits.size()) {
			assertThat(petVisits.get(i).getDate()).isEqualTo(mockVisits.get(j).getDate());
			i++;
			j--;
		}
		assertThat(j).isEqualTo(-1);
	}

	@Test
	public void testVisitsBetweenDates() {
		List<Visit> petVisits = mockPet.getVisitsBetween(LocalDate.of(2010, 1, 1), LocalDate.of(2010, 1, 3));
		assertThat(petVisits.size()).isEqualTo(1);

		petVisits = mockPet.getVisitsBetween(LocalDate.of(2009, 1, 1), LocalDate.of(2009, 1, 3));
		assertThat(petVisits.size()).isEqualTo(0);

		petVisits = mockPet.getVisitsBetween(LocalDate.of(2010, 1, 1), LocalDate.of(2020, 1, 3));
		assertThat(petVisits.size()).isEqualTo(mockVisits.size());

		petVisits = mockPet.getVisitsBetween(LocalDate.of(2019, 1, 1), LocalDate.of(2020, 1, 3));
		assertThat(petVisits.size()).isEqualTo(2);

		petVisits = mockPet.getVisitsBetween(LocalDate.of(2020, 1, 3), LocalDate.of(2021, 1, 3));
		assertThat(petVisits.size()).isEqualTo(0);
	}

	@Test
	public void testVisitsUntilAge() {
		List<Visit> petVisits = mockPet.getVisitsUntilAge(1);
		assertThat(petVisits.size()).isEqualTo(1);

		petVisits = mockPet.getVisitsUntilAge(5);
		assertThat(petVisits.size()).isEqualTo(5);

		petVisits = mockPet.getVisitsUntilAge(12);
		assertThat(petVisits.size()).isEqualTo(mockVisits.size());
	}

	@Test(expected = AssertionError.class)
	public void testNoVisitsBeforeBirth() {
		Pet pet = new Pet();
		pet.setBirthDate(LocalDate.of(2015, 1, 1));
		pet.addVisit(mockVisits.get(0));
		assertThat(pet.getVisits().size()).as("Should not be able to add visits before date of birth").isNotEqualTo(1);
	}

	@Test
	public void testRemovingVisits() {
		List<Visit> petVisits = mockPet.getVisits();
		assertThat(petVisits.size()).isEqualTo(mockVisits.size());

		mockPet.removeVisit(mockVisits.get(0));
		petVisits = mockPet.getVisits();
		assertThat(petVisits.size()).isEqualTo(mockVisits.size() - 1);

		mockPet.removeVisit(mockVisits.get(0));
		petVisits = mockPet.getVisits();
		assertThat(petVisits.size()).isEqualTo(mockVisits.size() - 1);

		mockPet.removeVisit(mockVisits.get(1));
		mockPet.removeVisit(mockVisits.get(2));
		petVisits = mockPet.getVisits();
		assertThat(petVisits.size()).isEqualTo(mockVisits.size() - 3);
	}


	static private Owner makeMockOwner() {
		Owner owner = new Owner();
		owner.setFirstName("rastin");
		owner.setLastName("soraki");
		return owner;
	}

	static private PetType makeMockPetType() {
		PetType petType = new PetType() {
			{
				setName("Cat");
			}
		};
		return petType;
	}

	static protected ArrayList<Visit> makeMockVisits() {
		ArrayList<Visit> visitsList = new ArrayList<>();
		for(int i = 2010; i < 2021; i++) {
			Visit visit = new Visit();
			visit.setDate(LocalDate.of(i, 1, 2));
			visitsList.add(visit);
		}
		return visitsList;
	}

	static private Pet makeMockPet() {
		Pet pet = new Pet();
		pet.setBirthDate(LocalDate.of(2009, 1, 8));
		pet.setOwner(mockOwner);
		pet.setType(mockPetType);
		pet.setName("shakespeare");

		for(Visit visit: mockVisits) {
			pet.addVisit(visit);
		}
		return pet;
	}
}

