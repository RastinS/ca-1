package org.springframework.samples.petclinic.owner;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.springframework.samples.petclinic.visit.Visit;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(Parameterized.class)
public class getVisitsUntilAgeTestParametrized {

	public int age, expected;

	private Pet pet;
	@Before
	public void setup() {
		pet = new Pet();
		pet.setBirthDate(LocalDate.of(2009, 1, 8));
		for(Visit visit: PetTest.makeMockVisits()) {
			pet.addVisit(visit);
		}
	}

	public getVisitsUntilAgeTestParametrized(int age, int expected) {
		this.age = age;
		this.expected = expected;
	}

	@Parameters
	public static Collection<Object[]> parameters() {
		return Arrays.asList(new Object[][] { {1, 1}, {7, 7}, { 3, 3}, {10, 10}, {15, 11}});
	}

	@Test
	public void getVisitsUntilAgeTest() {
		List<Visit> petVisits = pet.getVisitsUntilAge(age);
		assertThat(petVisits.size()).isEqualTo(expected);
	}
}
